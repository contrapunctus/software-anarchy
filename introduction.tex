  \section{Introduction}

  \subsection{Preface}
  
  This book covers our interpretations of programming as it
  relates to \emph{social ecology}, a philosophical theory that
  suggests many forms of dominion have the same causes and methods, and
  \emph{peer production}, a self-organising mode of production, and
  how their application can support a radical programmer exceptionally
  well.

  A \emph{liberatory technology} provides an improvement in the
  work which can be done with some amount of effort and time. A community
  which utilises such a technology well can perform much more work
  independently. We believe a liberatory technology for programmers
  takes the form of a \emph{dynamic environment}, which is a property of
  some programming languages and implementations that allows for many
  forms of modification and inspection at any time while a program is
  running.

  We will also investigate the supposed disadvantages of a decentralised
  development strategy, and how they may instead be used to the advantage
  of a community. We also will present why it is desirable to distribute
  source materials, especially in software development, and how shunning
  distributing source materials as anti-capitalist ``praxis'' is
  nonsensical.
  Finally, we will discuss what an anti-hierarchical view of some
  communication networks can bring to light; especially with community
  moderation systems, the property relationships a user conjures with
  their environment, and how to remove as many constraints on the
  means of communication and presentation as possible.
  
  We hold the belief that software peer production while rejecting the
  potential of dynamic environments is, at best, incoherent, and that a
  user of dynamic environments outside peer production may well have
  problems applying the environment as much as they could have. We try
  to achieve a few aims with this book: to introduce the reader to an
  idea of \emph{software anarchy}, to investigate some common
  opposition to it, and to provide a wide selection of resources that a
  reader (whom can produce enough free time) can read to help develop
  their own approaches.

  \subsection{Beginning a constant analysis}

  \textbf{Every piece of code you've ever written is a lie.}

  Every day we encounter issues with software, from flaws to bugs to
  minute nitpicks. Every day, we imagine how software could change, so
  that using it could be easier for everyone. Every day we experience
  more and more of the true nature of the software world. Every day we
  see that, in this world, things cannot be changed, and so eventually
  we learn to no longer imagine. Every day we contribute a million more
  lies, a million more arrows to fall back from the sky. Every day we
  bring ourselves to accept that this is how it is, how things
  are. Every day We say this is how it has to be.

  \textbf{Every piece of code you've ever written is a lie.} Why?
  Why do we say this is how it has to be?
  Why do we bring ourselves to accept that this is how it is?
  Why can't we just change...

  This despair in software production parallels the despair one might
  have with the political climate, the world at large. Anarchism is
  the process of constantly analysing all hierarchy and actively
  moving to dismantle them, and an anarchist framework should be
  applied to software. We should constantly analyse software: ``Why is
  this so complicated?'' ``Why can't I just express what's in my
  head?''  We should actively move to dismantle complicated software
  and develop new ways to express problems and ideas.

  One might wonder about the practicality of undertaking such an
  analysis, as there are surely are some problems that simply cannot be
  solved; some things need to be completely rewritten, and some things
  just won't be extended in some ways.
  We have sunken so many resources into non-general solutions and made
  massive mountains of code, but it doesn't have to be this way. Another
  world is possible, even if it starts out a small one. We will describe
  a strategy for establishing \emph{software anarchy}, our application
  of anarchism to software development.

  While it would be inappropriate to claim any ownership or other
  significance in presenting these ideas, many of the other radical
  software development projects fall short in their analyses of
  hierarchies, relations between programmers, users and other users, and
  so on, and are certainly far from ``what [we] had in mind''. In
  contrast, we believe software anarchy provides a cohesive critique of
  the current state of software development, ``radical'' or otherwise,
  and would be integral to a praxis that actually liberates its
  practitioners from hierarchy established by software and its
  applications.

  \subsection{The last constant analysis}

  It appears radical programmers frequently lose their grounding on
  what forms of organisation are right or wrong while on the Internet.
  We hear one group establish and espouse the direct democracy they
  formed, and then go on to write about ``the tyranny of the
  majority'' elsewhere. We read someone cussing out intellectual
  property, and then see them express support for a supposedly ethical
  business model that promotes more information hiding. It is now
  common to suggest some ways of improving online discussion that push
  for more asymmetry and hierarchy which would be unthinkable in
  physical spaces. This kind of contradiction would only make sense if
  there was something intrinsic to online spaces that makes them
  totally different, but we are headed to a future where there may be
  nothing intrinsic to base an excuse on.

  We cannot deny that cyber-space and the ``real'' world (or
  meat-space or whatever you want to call it) are becoming one and the
  same. Many of our mundane tasks which affect our physical world and
  identity are scheduled and performed online, and online spaces tend
  to hit barriers which are either the same as, or even worse forms
  of, issues that affect physical spaces. It should appear that a view
  on how to maintain an online society and a view on how to maintain
  an offline society should converge; but the opposite may well be
  occuring. For eaxmple, when an egalitarian community has continued
  to exist, it is always suggested that squashing the dynamics and
  independent experiments in the community, in favour of coercing the
  participants to use one set of techniques and produce one set of
  solutions, in order to support of a vague notion of coherence.
  Depending on the social standing and marketing skills of the person
  who suggests the solution, it is sometimes almost taken seriously.
  
  It is also in vogue to lose the control of, and lose abstract reasoning
  about a program. One programmer once wrote the words ``The era of dynamic
  languages is over. There is currently a race to the bottom\ldots''
  There is not a bit of resistance or disgust at that observation. Our
  plane is losing altitude quickly, and we simply couldn't give a shit.
  ``Brace for impact. I'm not going to pull on the control wheel\ldots''
  Such apathy is better than the average response, sadly. We are pushing
  ourselves to imagine how to design programs for smaller and less
  featureful machines than most of us will ever be tasked with
  programming, and basing our decisions for how to program larger
  machines on that. The target for what constitutes a \emph{high
    level language} has moved so far that it is acceptable to have
  ``abstractions'' which will somehow get us out of performing
  partly-automated static analysis on our programs, because such itty
  bitty machines cannot support the runtime features that would
  otherwise be used. However, those abstractions cannot meaningfully be
  formed, and if they could be formed, they still would put their user in
  a worse position. 

  This form of meaningless and purposeless minimalism allows no useful
  work to be performed, and echoes the plan-everything-ahead design
  techniques we learnt and scoffed at in school, which we never saw
  or used again, because of how useless they were. We accept having to
  approximate clairvoyance, in order to micro-optimise a large system,
  and to validate the many interactions in them. With these costs,
  experimentation cannot possibly be done after the initial design is
  made, and the capability to make any progress beyond what has already
  been done is a joke. Bob Barton's infamous observation applies here:
  although there will be no priests, will radical programming become a
  low cult?

  \subsection{Acknowledgements}

  We would like to thank Robert Strandh and Selwyn Simsek, among many
  \texttt{\#sicl} and \texttt{\#\#symbolics2} participants, for
  reviewing earlier drafts of this book. We are currently sending out
  some drafts for proofreading; you can have your name here if you are
  reading this draft, and you like having your name in acknowledgement
  sections.
  
  Please note that this book is licensed under the Cooperative Software
  License (written in Appendix A), and you are given permission and most
  welcome to modify and reproduce this book in many ways, provided that
  they are not used for profit-seeking in a hierarchical organisation.
